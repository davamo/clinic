# Dump of table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;
CREATE TABLE `clinic`.`user` (
  `id` INT NOT NULL,
  `name` VARCHAR(100) NULL,
  `email` VARCHAR(45) NULL,
  `password` VARCHAR(45) NULL,
  PRIMARY KEY (`id`));


# Dump of table drug
# ------------------------------------------------------------
DROP TABLE IF EXISTS `drug`;
CREATE TABLE `clinic`.`drug` (
  `id` INT NOT NULL,
  `name` VARCHAR(100) NULL,
  `approved` TINYINT NULL,
  `min_dose` INT NULL,
  `max_dose` INT NULL,
  `available_at` DATETIME NULL,
  PRIMARY KEY (`id`));

# Dump of table vaccination
# ------------------------------------------------------------
DROP TABLE IF EXISTS `vaccination`;
CREATE TABLE `clinic`.`vaccination` (
  `id` INT NOT NULL,
  `name` VARCHAR(100) NULL,
  `drug_id` INT NULL,
  `dose` INT NULL,
  `date` DATETIME NULL,
  PRIMARY KEY (`id`));