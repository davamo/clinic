import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateVaccinationDto } from './dto/create-vaccination.dto';
import { UpdateVaccinationDto } from './dto/update-vaccination.dto';
import { Vaccination } from './entities/vaccination.entity';

@Injectable()
export class VaccinationService {
  constructor(
    @InjectRepository(Vaccination)
    private readonly vaccinationRepository: Repository<Vaccination>,
  ) {}

  async create(vaccinationObject: CreateVaccinationDto) {
    const vaccination = this.vaccinationRepository.create(vaccinationObject);
    return await this.vaccinationRepository.save(vaccination);
  }

  async update(id: number, updateVaccinationDto: UpdateVaccinationDto) {
    const data = await this.vaccinationRepository.update(
      id,
      updateVaccinationDto,
    );
    return data;
  }

  findAll() {
    return this.vaccinationRepository.find();
  }

  remove(id: number) {
    const data = this.vaccinationRepository.delete({ id });
    return data;
  }
}
