import { Column } from 'typeorm';
export class CreateVaccinationDto {
  @Column()
  name: string;

  @Column()
  drug_id: string;

  @Column()
  dose: string;

  @Column()
  date: string;
}
