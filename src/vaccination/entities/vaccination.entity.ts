import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'vaccination' })
export class Vaccination {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  drug_id: string;

  @Column()
  dose: string;

  @Column()
  date: string;
}
