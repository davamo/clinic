import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateDrugDto } from './dto/create-drug.dto';
import { UpdateDrugDto } from './dto/update-drug.dto';
import { Drug } from './entities/drug.entity';

@Injectable()
export class DrugsService {
  constructor(
    @InjectRepository(Drug)
    private readonly drugRepository: Repository<Drug>,
  ) {}

  async create(drugObject: CreateDrugDto) {
    const drug = this.drugRepository.create(drugObject);
    return await this.drugRepository.save(drug);
  }

  async update(id: number, updateDrugDto: UpdateDrugDto) {
    const data = await this.drugRepository.update(id, updateDrugDto);
    return data;
  }

  findAll() {
    return this.drugRepository.find();
  }

  remove(id: number) {
    const data = this.drugRepository.delete({ id });
    return data;
  }
}
