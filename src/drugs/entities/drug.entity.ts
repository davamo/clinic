import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'drug' })
export class Drug {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  approved: string;

  @Column()
  min_dose: string;

  @Column()
  max_dose: string;

  @Column()
  available_at: string;
}
