import { Column } from 'typeorm';
export class CreateDrugDto {
  @Column()
  name: string;

  @Column()
  approved: string;

  @Column()
  min_dose: string;

  @Column()
  max_dose: string;

  @Column()
  available_at: string;
}
