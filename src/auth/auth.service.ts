import { HttpException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { RegisterAuthDto } from './dto/register-auth.dto';
import { LoginAuthDto } from './dto/login-auth.dto';
import { hash, compareSync } from 'bcrypt';
import { AuthRepository } from './auth.repository';
import { UserEntity } from '../users/entities/user.entity';
import { TokenDto } from './dto/token.dto';
import { PayloadInterface } from './payload.interface';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly authRepository: AuthRepository,
    private jwtService: JwtService,
  ) {}

  async register(userObject: RegisterAuthDto) {
    const { password } = userObject;
    const plainTohash = await hash(password, 10);
    userObject = { ...userObject, password: plainTohash };
    const user = this.authRepository.create(userObject);
    return await this.authRepository.save(user);
  }

  async login(userObjectLogin: LoginAuthDto) {
    const { email, password } = userObjectLogin;
    const findUser = await this.authRepository.findOne({
      where: { email: email },
    });
    if (!findUser) throw new HttpException('USER_NOT_FOUND', 404);

    const checkPassword = await compareSync(password, findUser.password);
    if (!checkPassword) throw new HttpException('PASSWORD_INVALID', 403);

    const payload = { id: findUser.id, name: findUser.name };
    const token = this.jwtService.sign(payload);

    const data = {
      user: findUser,
      token,
    };
    return data;
  }

  /*async refresh(dto: TokenDto): Promise<any> {
    const findUser = await this.jwtService.decode(dto.token);
    const payload: PayloadInterface = {
      id: findUser[`id`],
      name: findUser[`name`],
      email: findUser[`email`],
    };
    const token = await this.jwtService.sign(payload);
    return token;
  }*/
}
