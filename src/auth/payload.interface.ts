export interface PayloadInterface {
  id: number;
  name: string;
  email: string;
}
